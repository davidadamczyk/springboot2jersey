package com.davidadamczyk.springboot2jersey;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Springboot2jerseyApplicationTests {

	@Test
	public void contextLoads() {
	}

}
