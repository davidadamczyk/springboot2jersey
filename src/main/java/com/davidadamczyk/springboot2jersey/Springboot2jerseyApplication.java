package com.davidadamczyk.springboot2jersey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot2jerseyApplication {

	public static void main(String[] args) {
		SpringApplication.run(Springboot2jerseyApplication.class, args);
	}
}
